package utfpr.ct.dainf.pratica;

import java.util.Comparator;
import java.util.Date;

/**
 * Linguagem Java
 * @author
 */
public class LancamentoComparator implements Comparator<Lancamento>{    
     
    public LancamentoComparator(){
        
    }
    
    @Override
    public int compare(Lancamento t1, Lancamento t2) {
        if(t1.getConta().equals(t2.getConta())){
            if(t1.getData().after(t2.getData())){
                return 1;
            } else if (t1.getData().before(t2.getData())){
                return -1;
            }
            else return 0;
        }   else return t1.getConta() - t2.getConta();       
    }
}
