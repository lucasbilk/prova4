
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import utfpr.ct.dainf.pratica.Lancamento;
import utfpr.ct.dainf.pratica.ProcessaLancamentos;

/**
 * IF62C Fundamentos de Programação 2
 * Avaliação parcial.
 * @author 
 */
public class Pratica {
 
    public static void main(String[] args) throws FileNotFoundException, IOException, ParseException {
        Scanner scanner = new Scanner(System.in);
        String caminho;
        ArrayList<Lancamento> lancamentos = new ArrayList<>();
        
        System.out.println("Digite o caminho completo para o arquivo");
        caminho = scanner.next();
        
        File arquivo = new File(caminho);
        
        ProcessaLancamentos arqLancamentos = new ProcessaLancamentos(arquivo);
        
        arqLancamentos.getLancamentos();
    }
    
    public static void exibeLancamentosConta(List<Lancamento> lancamentos, Integer conta) {
           
    }
 
}